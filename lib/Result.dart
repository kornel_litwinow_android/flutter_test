import 'package:flutter/material.dart';

 class Result extends StatelessWidget {
  /*
  Widget wyniku
  zawiera liczbe pkt i funkcje ktora zaczyna quiz od poczatku
   */

  final int resultScore;
  final Function resetHandler;

  Result(this.resultScore,this.resetHandler);

  String get resultPhrase{
    String resultText;
    resultText = "You got" + "\r" + resultScore.toString() + "\r"+ "points!";
    return resultText;
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(top: 20),
            child: Text(resultPhrase,
              textAlign: TextAlign.center,
              style: TextStyle(
              fontSize: 36,
              fontWeight: FontWeight.bold
              ),
            ),
          ),
          FlatButton(
            child: Text("Restart test",
            style: TextStyle(
                color: Colors.green,fontSize: 28),
            ),
            onPressed:resetHandler ,
          ),
        ],
      ),
    );
   }
 }
