import 'package:flutter/material.dart';

class Answer  extends StatelessWidget {

  /*
  Contener zwracajacy tresci odpowiedzi
  Odpowiedzi zawiera tresc i funkcje ktora zapisuje odpoweidz
   */

  final Function selectHandler;
  final String answerText;

  Answer(this.selectHandler, this.answerText);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.only(left: 15, top: 0, right: 15, bottom: 0),
      child: RaisedButton(
      color: Colors.green,
      textColor: Colors.white,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20)
      ),
      child: Text(answerText),
      onPressed: selectHandler,
     ),
    );
  }
}
