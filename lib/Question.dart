import 'package:flutter/material.dart';

class Question extends StatelessWidget {

  /*
  Jest to kontener zawierajacy tresc pytania
  Szerokosc na caly ekran, marginesy z kazdej strony po 20
  child to widget text
   */

  final String questionText;
  Question(this.questionText);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.all(20),
      child: Text(questionText,
      style: TextStyle(
          fontSize: 25,
          fontWeight: FontWeight.bold
      ),
          textAlign: TextAlign.center,
     ),
    );
   }
 }
