
import 'package:flutter/material.dart';
import './Quiz.dart';
import './Result.dart';


 void main(){
  runApp(MyApp());
 }

 class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget>createState(){
    return _MyAppState();
  }
}

 class _MyAppState extends State<MyApp> {


  final _questions = const[

    {"questionText":"Suite",
      "answers":
      [{"text": "Zestaw","score": 1},
      {"text":"False","score":0},
        {"text":"False","score":0},
        ],
    },

    {"questionText":"Particular",
      "answers":
      [{"text":"False","score":0},
       {"text":"False","score":0},
       {"text":"Szczególny","score":1},
      ],
    },

    {"questionText":"Distinct",
      "answers":
      [{"text":"False","score":0},
       {"text":"Odrębny","score":1},
       {"text":"False","score":0},
      ],
    },

  ];
  var _questionIndex = 0;
  var _totalScore = 0;

void _resetQuiz(){
  setState(() {
    _questionIndex = 0;
    _totalScore = 0;
  });

}

  void _answerQuestion(int score){
    _totalScore = _totalScore + score;
    setState(() {
      _questionIndex = _questionIndex + 1;
    });
    print(_questionIndex);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
        appBar: AppBar(
        backgroundColor: Colors.green,
        title: Text("English test"
        ),
    ),
        body: _questionIndex<_questions.length ? Quiz(
        answerQuestion: _answerQuestion,
        questionIndex: _questionIndex,
        questions: _questions,
        )
        : Result(_totalScore,_resetQuiz),

        ),
    );
  }
}